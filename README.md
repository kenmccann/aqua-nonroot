Currently all Aqua images are designed to run the root user inside the container.  This violates the strict PodSecurityPolicies of many organizations.  This project is aimed to run the Aqua infrastructure as a non-root user and discover any issues. 

# Challenges to solve
- /etc/resolv.conf cannot be modified by web-entrypoint.sh (remove options ndots)
- /opt/aquasec must be writeable by non-root
- /opt/aquascans is a volumemount from source Dockerfile.  It must be writeable by nonroot.

# Considerations
- Without access to docker.sock the console can *only* perform dockerless scanning.
- The path /opt/aquascans MUST be overridden by a runtime mount since it is a VOLUME in Aqua's source Dockerfile. 
- In kubernetes, you can mount /opt/aquascans as an emptydir which will be discarded with the pod