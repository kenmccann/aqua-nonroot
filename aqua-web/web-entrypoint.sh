#!/bin/sh

# Due to Alpine issue with DNS resolution we are deleting lines with "options ndots"
# From the /etc/reslov.conf.
# These lines might be added by orchestrator, like Openshift or Kubernetes.
#cp /etc/resolv.conf ~/resolv.conf
#sed '/options ndots/d' ~/resolv.conf > /etc/resolv.conf

#exec is needed in order to make the run be the parent process for accespting docker stop signal
exec /opt/aquasec/sedockweb
